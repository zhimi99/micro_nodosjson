package ec.edu.ups.micro.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.ups.micro.demo.models.RequestMessage;
import ec.edu.ups.micro.demo.models.Root;
import ec.edu.ups.micro.demo.models.SensorCO2;
import ec.edu.ups.micro.demo.services.SensorCo2Service;

@RestController
@RequestMapping("/co2")
public class Co2Controller {

	@Autowired
	SensorCo2Service sensorCo2Service;

	@PostMapping(path = "/save")
	public RequestMessage registrar(@RequestBody Root root) {
		return this.sensorCo2Service.registrarMedicionCo2(root);
	}
	
	@GetMapping(path = "/listarTodo")
	public Page<SensorCO2> listarTodo(@RequestParam(defaultValue = "10") int page, @RequestParam(defaultValue = "10") int size)  throws Exception {
		return this.sensorCo2Service.listarTodo(PageRequest.of(page, size));
	}

	@GetMapping(value = "/listarxfecha") // permite mostrar todos los nodos selecionando solo la fecha a buscar
	public Page<SensorCO2> listarSensoresPorFecha(@RequestParam String fecha,
			@RequestParam(defaultValue = "10") int page, @RequestParam(defaultValue = "10") int size) throws Exception {
		return this.sensorCo2Service.listarSensoresPorFecha(fecha, PageRequest.of(page, size));
	}

	@GetMapping(value = "/listarxnodo") // permite listar toda la data dependiendo del nodo seleccionado
	public Page<SensorCO2> listarNodos(@RequestParam String nombreDispositivo,
			@RequestParam(defaultValue = "10") int page, @RequestParam(defaultValue = "10") int size) {
		return this.sensorCo2Service.listaMedicionesPorNodo(nombreDispositivo, PageRequest.of(page, size));
	}

	@GetMapping(value = "/listar_fecha_nodo")
	public Page<SensorCO2> listar(@RequestParam String fecha, @RequestParam String nodo,
			@RequestParam(defaultValue = "10") int page, @RequestParam(defaultValue = "10") int size) throws Exception {
		return this.sensorCo2Service.listarMedicionesPorNodoyFecha(fecha, nodo, PageRequest.of(page, size));
	}

	@GetMapping(value = "/listar_entre_fecha_nodo")
	public Page<SensorCO2> listar(@RequestParam String fechaInicial, @RequestParam String fechaFinal,
			@RequestParam String nodo, @RequestParam(defaultValue = "10") int page,
			@RequestParam(defaultValue = "10") int size) throws Exception {
		return this.sensorCo2Service.listarMedicionesPorNodoyEntreFecha(fechaInicial, fechaFinal, nodo,
				PageRequest.of(page, size));
	}

	@GetMapping(value = "/listarTodosNodosEntreFechas") // permite listar todos los nodos
	public Page<SensorCO2> listarNodosEntreFechas(@RequestParam String fechaInicial, @RequestParam String fechaFinal,
			@RequestParam(defaultValue = "10") int page, @RequestParam(defaultValue = "10") int size) throws Exception {
		return this.sensorCo2Service.listarNodosEntreFechas(fechaInicial, fechaFinal, PageRequest.of(page, size));
	}

}
