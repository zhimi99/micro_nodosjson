package ec.edu.ups.micro.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ChirpstackTranslate {

	public static void main(String[] args) {
		SpringApplication.run(ChirpstackTranslate.class, args);
	}

}
