package ec.edu.ups.micro.demo.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ec.edu.ups.micro.demo.models.RequestMessage;
import ec.edu.ups.micro.demo.models.Root;
import ec.edu.ups.micro.demo.models.SensorCO2;
import ec.edu.ups.micro.demo.repository.SensorCo2Repository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class SensorCo2Service {

	@Autowired
	SensorCo2Repository sensorCo2Repository;

	public RequestMessage registrarMedicionCo2(Root root) {
		JSONObject json = new JSONObject(root.getObjectJSON());

		SensorCO2 sensorCO2 = new SensorCO2();

		sensorCO2.setFecha(root.getRxInfo().get(0).getTime());
		sensorCO2.setFechaBusq(root.getRxInfo().get(0).getTime());
		sensorCO2.setHora(root.getRxInfo().get(0).getTime());
		sensorCO2.setDeviceName(root.getDeviceName());
		sensorCO2.setCo2_A(json.getDouble("Co2_A"));
		sensorCO2.setCo2_b(json.getDouble("Co2_b"));

		RequestMessage rm = new RequestMessage();

		try {
			sensorCo2Repository.save(sensorCO2);
			rm.setCode("0");
			rm.setMessage("Peticion ingresada correctamente: " + json.toString());
		} catch (Exception e) {
			rm.setCode("1");
			rm.setMessage("Fallo WS" + e.getMessage());
		}
		return rm;
	}

	
	public Page<SensorCO2> listarTodo(Pageable pageable) throws Exception {
		
		Page<SensorCO2> listarSensoresPorFecha = sensorCo2Repository.findAll(pageable);
		
		return listarSensoresPorFecha;
	}
	
	
	public Page<SensorCO2> listarSensoresPorFecha(String fecha, Pageable pageable) throws Exception {
		
		Date sdf = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
		Page<SensorCO2> listarSensoresPorFecha = sensorCo2Repository.listarSensoresPorFecha(sdf, pageable);
		//Type listType = new TypeToken<List<SensorCO2>>() {
		//}.getType();

		//System.out.println(listarSensoresPorFecha.size());
		//Gson gson = new Gson();
		//String json = gson.toJson(listarSensoresPorFecha, listType);

		//return json;
		return listarSensoresPorFecha;
	}

	
	
	public Page<SensorCO2> listaMedicionesPorNodo(String nombreDispositivo, Pageable pageable) {
		Type listType = new TypeToken<List<SensorCO2>>() {
		}.getType();
		Page<SensorCO2> lista = sensorCo2Repository.listarMedicionesPorDispositivo(nombreDispositivo, pageable);
		//Gson gson = new Gson();
		//String json = gson.toJson(lista, listType);

		//return json;
		return lista;
	}

	
	
	public Page<SensorCO2> listarMedicionesPorNodoyFecha(String fecha, String nodo, Pageable pageable) throws Exception {
		Date sdf1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
		Page<SensorCO2> listarSensoresPorFecha = sensorCo2Repository.listarMedicionesPorDispositivoyFecha(nodo, sdf1, pageable);
//		Type listType = new TypeToken<List<SensorCO2>>() {
//		}.getType();
//
//		Gson gson = new Gson();
//		String json = gson.toJson(listarSensoresPorFecha, listType);
//
//		return json;
		return listarSensoresPorFecha;

	}
	
	
	
	public Page<SensorCO2> listarMedicionesPorNodoyEntreFecha(String fechaInicio, String fechaFin, String nodo, Pageable pageable) throws Exception {
		
		Date sdf1 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaInicio);
		Date sdf2 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaFin);
		Page<SensorCO2> listarSensoresPorFecha = sensorCo2Repository.listarMedicionesPorDispositivoyEntreFecha(nodo, sdf1, sdf2, pageable);
		
		return listarSensoresPorFecha;

	}
	
	public Page<SensorCO2> listarNodosEntreFechas(String fechaInicial, String fechaFinal, Pageable pageable) throws Exception {
		
		Date sdf1 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaInicial);
		Date sdf2 = new SimpleDateFormat("yyyy-MM-dd").parse(fechaFinal);
		Page<SensorCO2> listarSensoresPorFecha = sensorCo2Repository.listarTodosSensoresPorRangoDeFechas(sdf1, sdf2, pageable);
		
		return listarSensoresPorFecha;

	}
	
	

}
