package ec.edu.ups.micro.demo.repository;

import ec.edu.ups.micro.demo.models.SensorCO2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Date;


public interface SensorCo2Repository extends JpaRepository<SensorCO2, Long> {


    @Query ("SELECT n from SensorCO2 n where n.fecha = :fechaParam")
    Page<SensorCO2> listarSensoresPorFecha(@Param("fechaParam") Date fecha, Pageable pageable);

    @Query ("SELECT n from SensorCO2 n where n.deviceName = :nombreDispositivo")
    Page<SensorCO2> listarMedicionesPorDispositivo(@Param("nombreDispositivo") String nombreDispositivo, Pageable pageable);

    @Query ("SELECT n from SensorCO2 n where n.deviceName like :nombreDispositivo and n.fechaBusq = :fecha ORDER BY id DESC")
    Page<SensorCO2> listarMedicionesPorDispositivoyFecha(String nombreDispositivo, Date fecha, Pageable pageable);
    
    @Query ("SELECT n from SensorCO2 n where n.deviceName like :nombreDispositivo and n.fechaBusq between :fechaInicial and :fechaFinal ORDER BY id DESC")
    Page<SensorCO2> listarMedicionesPorDispositivoyEntreFecha(String nombreDispositivo, Date fechaInicial, Date fechaFinal, Pageable pageable);
    
    @Query ("SELECT n from SensorCO2 n where n.fecha between :fechaInicial and :fechaFinal ORDER BY id DESC")
    Page<SensorCO2> listarTodosSensoresPorRangoDeFechas(@Param("fechaInicial") Date fechaInicial, @Param("fechaFinal") Date fechaFinal, Pageable pageable);

   
}
