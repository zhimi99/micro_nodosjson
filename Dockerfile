FROM openjdk:12
VOLUME /tmp
ADD ./target/demo-0.0.1-SNAPSHOT.jar servicio-nodoco2.jar
ENTRYPOINT ["java","-jar","/servicio-nodoco2.jar"]